import OverViewboard from "Pages/OverViewboard/overViewBoard";
import RouteModel from "Models/IRouteModel";
import SignUp from "Pages/SignUp/signUp";
import SignIn from "Pages/SignIn/signIn";
import CreateSurvey from "Pages/CreateSurvey/createSurvey";
import OverViewboardResponder from "Pages/OverViewBoardResponder/overViewBoardResponder";
import ResponderSurveyQuestion from "Pages/ResponderSurveyQuestion/responderSurveyQuestion";
import AnalyticBoard from "Pages/Analytics/analyticsBoard";
import LogoutComponent from "Pages/LogoutComponent/logoutComponent";

const config: Array<RouteModel> = [
  {
    path: "/signup",
    exact: true,
    auth: false,
    component: SignUp,
  },
  {
    path: "/signin",
    exact: true,
    auth: false,
    component: SignIn,
  },
  {
    path: "/logout",
    exact: true,
    auth: false,
    component: LogoutComponent,
  },



  {
    path: "/",
    exact: true,
    auth: true,
    variant: 1,
    component: OverViewboard,
  },

  {
    path: "/create-survey",
    exact: true,
    auth: true,
    variant: 1,
    component: CreateSurvey,
  },


  {
    path: "/responder",
    exact: true,
    auth: true,
    variant: 2,
    component: OverViewboardResponder,
  },
  {
    path: "/responder-surveyquestion/:id",
    exact: true,
    auth: true,
    variant: 2,
    component: ResponderSurveyQuestion,
  },


  {
    path: "/analytics/:id",
    exact: true,
    auth: true,
    component: AnalyticBoard,
  },
];

export default config;
