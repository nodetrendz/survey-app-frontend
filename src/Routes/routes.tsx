import ISurveyContext from "Http/com/context/ISurveyContext";
import SurveyContext from "Http/com/context/SurveyContext";
import AuthService from "Http/Service/AuthService";
import React, { useContext, useEffect, useState } from "react";
import { Router, Switch, Route, Redirect, useHistory } from "react-router-dom";
import config from "./config";

const ProtectedRoute: React.FC = ({ component: Component, ...params }: any) => {

    const [authenticated, setAuthenticated] = useState<boolean | undefined>(undefined);

    useEffect(() => {
        let mounted = true;
        
        if (mounted) {
            AuthService.fetch_me().then((me: any) => {
                setAuthenticated(me !== undefined);
            }).catch((err: any) => setAuthenticated(false));
        }

        return () => {
            mounted = false;
        };
    }, []);

    return <Route
        {...params}
        render={(routeProps: any) => {
            if (authenticated === false) {
                return <Redirect
                    to={{
                        pathname: "/signin",
                        state: { referrer: routeProps.location },
                    }}
                />
            } 
            return <Component {...routeProps} />;
        }}
    />
};

const Routes: React.FC = () => {
    const history = useHistory();

    const context: ISurveyContext = useContext(SurveyContext);

    return (
        <Router history={history}>
            <Switch>
                {config.map((route: any, i) => {
                    const { component: Component, ...params } = route;
                    if (route.auth) {
                        if (route.variant && context.user) {
                            if (route.variant === context.user.variant.id) {
                                console.log('we are allowed in', route.variant, context.user.variant.id);
                            } else {
                                console.log('we are not allowed in', route.variant, context.user.variant.id);
                            }    
                        }
                        
                        return <ProtectedRoute component={Component} key={i} {...params} />;
                    } else {
                        return (
                            <Route
                                key={i}
                                {...params}
                                render={(routeProps: any) => <Component {...routeProps} />}
                            />
                        );
                    }
                })}
            </Switch>
        </Router>
    );
};

export default Routes;