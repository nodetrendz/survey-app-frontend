import axios from "axios";
import IAuthUser from "Http/com/models/requests/auth/IAuthUser";
import IForgotPassword from "Http/com/models/requests/auth/IForgotPassword";
import IRegisterUser from "Http/com/models/requests/auth/IRegisterUser";
import IResetPassword from "Http/com/models/requests/auth/IResetPassword";
import IAuth from "Http/com/models/responses/auth/IAuth";
import IUser from "Http/com/models/responses/auth/IUser";
import INotification from "Models/INotification";
import HttpService from ".";

class AuthService extends HttpService {
    constructor() {
        super();
        axios.create({ baseURL: process.env.REACT_APP_API_ROUTE });
    }

    public static fetch_me = async (): Promise<IUser | undefined> => {
        let response: IUser | undefined = undefined;
        try {
            const headers = {
                Authorization: `Bearer ${HttpService.getToken()}`,
            };
            const {
                data: { data },
            } = await HttpService.getRequest("/auth/fetch-me", {}, headers);
            response = data;
            
        } catch (exception: any) {}
        return response;
    };

    public static logout = async (): Promise<INotification> => {
        let response: INotification = {status: undefined, text: undefined};

        try {
            const {data: {message, status}} = await HttpService.getRequest('/auth/logout-user', {}, AuthService.fetchHeaders());
            response.status = status;
            response.text = message;
        } catch (err: any) {}

        return response
    };

    public static authenticate_user = async (
        request: IAuthUser
    ): Promise<IAuth> => {
        let response: IAuth = { status: false, token: "" };

        try {
            const {
                data: { status, data, message },
            } = await HttpService.postRequest(
                "/auth/authenticate-user",
                request
            );

            if (status === true && process.env.REACT_APP_API_TOKEN) {
                // store token in localstorage
                localStorage.setItem(process.env.REACT_APP_API_TOKEN, data);
            }
            response = { status, token: data, message };
        } catch (exception: any) {
            response.message = exception?.response.data?.message ?? "";
        }
        return response;
    };

    public static register_user = async (
        request: IRegisterUser
    ): Promise<boolean> => {
        let response: boolean = false;
    
        try {
            const { data: { status }} = await HttpService.postRequest('/auth/register-user', request);
            response = status;
        } catch (err: any) {}

        return response;
    };

    public static forgot_password = async (
        request: IForgotPassword
    ): Promise<any> => {
        return undefined;
    };

    public static reset_password = async (
        request: IResetPassword
    ): Promise<any> => {
        return undefined;
    };

    public static fetchHeaders = () => {
        const headers = {
            Authorization: `Bearer ${HttpService.getToken()}`,
        };
        return headers;
    };
}

export default AuthService;
