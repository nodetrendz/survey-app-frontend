import ISurvey from "Http/com/models/responses/coordinator/ISurvey";
import ICompleteSurvey from "Http/com/models/requests/respondent/ICompleteSurvey";
import ICompleteSurveyResponse from "Http/com/models/requests/respondent/ICompleteSurveyResponse";
import HttpService from ".";
import AuthService from "./AuthService";
import ISurveyListItem from "Http/com/models/responses/coordinator/ISurveyListItem";

class RespondentService {
    public static get_surveys = async (): Promise<Array<ISurveyListItem>> => {
        let response = [];
        try {
            const { data: {data}} = await HttpService.getRequest('/respondent/get-surveys', {}, AuthService.fetchHeaders());
            response = data;
        } catch (err: any) {}
        return response;
    };

    public static can_take_survey = async (id: string): Promise<any> => {
        let response = undefined;
        try {
            const { data: { status, data, message}} = await HttpService.getRequest(`/respondent/can-take-survey/${id}`, {}, AuthService.fetchHeaders());
            response = { status, data, message};
        } catch (err: any) {}
        return response;
    };

    public static get_open_surveys = async (): Promise<Array<ISurvey>> => {
        return [];
    };

    public static get_closed_surveys = async (
        id: number
    ): Promise<ISurvey | undefined> => {
        return undefined;
    };

    public static get_open_survey_by_id = async (id: string): Promise<any> => {
        let response = undefined;
        try {
            const { data: {data}} = await HttpService.getRequest(`/respondent/get-open-survey/${id}`, {}, AuthService.fetchHeaders());
            response = data;
        } catch (err: any) {}
        return response;
    };

    public static get_closed_survey_by_id = async (): Promise<
        ISurvey | undefined
    > => {
        return undefined;
    };

    public static complete_survey = async (
        payload: ICompleteSurvey
    ): Promise<ICompleteSurveyResponse | undefined> => {
        let response = undefined;
        try {
            const { data: { status, message }} = await HttpService.postRequest(`/respondent/complete-survey`, payload, AuthService.fetchHeaders());
            response = {status, message};
        } catch (err: any) {}
        return response;
    };
}

export default RespondentService;
