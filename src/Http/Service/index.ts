import axios from "axios";

class HttpService {

    /**
     * Method to send post request
     *
     * @param string url - the url to send the request to.
     * @param any payload - payload to send over to the server.
     * @param any headers - headers for the request
     *
     * @return Promise<any> response
     */
    public static postRequest = async (url: string, payload: any, headers: any = null): Promise<any> => {
        return axios.post(`${process.env.REACT_APP_API_ROUTE}${url}`, payload, { headers: headers });
    };

    /**
     * Method to send get request
     *
     * @param string url - the url to send the request to.
     * @param any payload - payload to send over to the server.
     * @param any headers - headers for the request
     *
     * @return Promise<any> response
     */
    public static getRequest = async (url: string, payload: any = {}, headers: any = null): Promise<any> => {
        return axios.get(`${process.env.REACT_APP_API_ROUTE}${url}`, { params: payload, headers: headers });
    };


    public static getToken = () => process.env.REACT_APP_API_TOKEN && localStorage.getItem(process.env.REACT_APP_API_TOKEN);
}

export default HttpService;
