import ISurveyContext from "Http/com/context/ISurveyContext";

class NotificationService {
    static handleNotification = (
        context: ISurveyContext,
        authenticated: any
    ) => {
        // set context values
        context.notification.status = authenticated.status;
        context.notification.text = authenticated.message;

        return setTimeout(() => {
            // clear notification
            context.notification.status = undefined;
            context.notification.text = undefined;
            return context;
        }, 1000);
    };
}

export default NotificationService;
