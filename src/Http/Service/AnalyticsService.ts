import HttpService from ".";

class AnalyticsService extends HttpService {
    public static get_survey = async (id: string): Promise<any> => {
        let response: any = [];

        try {
            const headers = {
                Authorization: `Bearer ${HttpService.getToken()}`,
            };
            const {
                data: { data },
            } = await HttpService.getRequest(
                `/analytics/get-survey/${id}`,
                {},
                headers
            );
            response = data;
        } catch (exception: unknown) {}

        return response;
    };
}

export default AnalyticsService;
