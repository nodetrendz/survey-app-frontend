import IDefineSurvey from "Http/com/models/requests/coordinator/IDefineSurvey";
import ISurvey from "Http/com/models/responses/coordinator/ISurvey";
import ISurveyResponse from "Http/com/models/responses/coordinator/ISurveyResponse";
import HttpService from ".";
import AuthService from "./AuthService";

class CoordinatorService extends HttpService {
    public static close_survey = async (id: number): Promise<boolean> => {
        let response = false;

        try {
            const {data: {status}} = await HttpService.getRequest(`/admin/close-my-survey/${id}`, {}, AuthService.fetchHeaders());
            response = status;
        } catch (exception: unknown) {}

        return response;
    };

    public static define_survey = async (
        request: IDefineSurvey
    ): Promise<ISurveyResponse | undefined> => {
        let response: ISurveyResponse | undefined = undefined;

        try {
            const headers = {
                Authorization: `Bearer ${HttpService.getToken()}`,
            };
            const {
                data: { data, status },
            } = await HttpService.postRequest(
                "/admin/define-survey",
                request,
                headers
            );
            response = { status, survey: data };
        } catch (exception: any) {}

        return response;
    };

    public static fetch_my_survey_by_id = async (
        id: number
    ): Promise<ISurvey | undefined> => {
        return undefined;
    };

    public static fetch_my_active_surveys = async (): Promise<
        ISurvey | undefined
    > => {
        return undefined;
    };

    public static fetch_my_surveys_counter = async (): Promise<
        ISurvey | undefined
    > => {
        let response: any = {};
        try {
            const headers = {
                Authorization: `Bearer ${HttpService.getToken()}`,
            };
            const {data: {data}} = await HttpService.getRequest(
                "/admin/get-my-surveys-counter",
                {},
                headers
            );
            response = data;
        } catch (err: any) {}
        return response;
    };

    public static fetch_my_surveys = async (): Promise<Array<ISurvey>> => {
        let response: Array<ISurvey> = [];

        try {
            const headers = {
                Authorization: `Bearer ${HttpService.getToken()}`,
            };
            const {
                data: { data },
            } = await HttpService.getRequest(
                "/admin/get-my-surveys",
                {},
                headers
            );
            response = data;
        } catch (exception: unknown) {}

        return response;
    };
}

export default CoordinatorService;
