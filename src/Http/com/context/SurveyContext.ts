import React from "react";
import ISurveyContext from "./ISurveyContext";

export const question_models: ISurveyContext = {
    title: "",
    notification: {},
    answers: {},
    questionModels: [],
};

export const SurveyContext = React.createContext(question_models);

export default SurveyContext;
