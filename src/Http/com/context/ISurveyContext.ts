import INotification from "Models/INotification";
import QuestionModel from "../models/requests/coordinator/formModels/QuestionModel";
import IUser from "../models/responses/auth/IUser";

interface ISurveyContext {
    title: string;
    user?: IUser;
    answers: any;
    notification: INotification;
    questionModels: QuestionModel[];
}

export default ISurveyContext;
