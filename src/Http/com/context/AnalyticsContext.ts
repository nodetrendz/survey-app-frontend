import React from "react";
import ISurveyContext from "./ISurveyContext";
import IAnalyticsContext from "./IAnalyticsContext";

export const data_models: IAnalyticsContext = {
    title: "",
    notification: {},
    tableData: {},
    pieData: {},
    barData: {},
};

const AnalyticsContext = React.createContext(data_models);

export default AnalyticsContext;
