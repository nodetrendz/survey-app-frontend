import React, { useReducer, Dispatch } from "react";
import INotification from "./INotificationContext";
export function createCtx<StateType, ActionType>(
  reducer: React.Reducer<StateType, ActionType>,
  initialState: StateType
) {
  const defaultDispatch: React.Dispatch<ActionType> = () => initialState; // we never actually use this
  const ctx = React.createContext({
    state: initialState,
    dispatch: defaultDispatch, // just to mock out the dispatch type and make it not optioanl
  });
  function Provider(props: React.PropsWithChildren<{}>) {
    const [state, dispatch] = React.useReducer<
      React.Reducer<StateType, ActionType>
    >(reducer, initialState);
    return <ctx.Provider value={{ state, dispatch }} {...props} />;
  }
  return [ctx, Provider] as const;
}
// usage
const initialState = {
  error: true,
  success: true,
};
type AppState = typeof initialState;
type Action =
  | { type: "SET_SUCCESS"; payload: any }
  | { type: "SET_ERROR"; payload: any };

function reducer(state: AppState, action: Action): AppState {
  switch (action.type) {
    case "SET_SUCCESS":
      return { ...state, success: action.payload };
    case "SET_ERROR":
      return { ...state, error: action.payload };
    default:
      throw new Error();
  }
}

export const NotificationContext = React.createContext<{
  state: INotification;
  dispatch: Dispatch<any>;
}>({
  state: initialState,
  dispatch: () => null,
});

const NotificationProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <NotificationContext.Provider value={{ state, dispatch }}>
      {children}
    </NotificationContext.Provider>
  );
};

export default NotificationProvider;
