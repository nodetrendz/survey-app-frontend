

interface INotificationContext {
    error: boolean;
    success: boolean;
}

export default INotificationContext;
