import INotification from "Models/INotification";
import QuestionModel from "../models/requests/coordinator/formModels/QuestionModel";
import IUser from "../models/responses/auth/IUser";

interface IAnalyticsContext {
    title: string;
    tableData: any;
    pieData: any;
    barData: any;
    notification: INotification;
}

export default IAnalyticsContext;
