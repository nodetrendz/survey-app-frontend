interface IAuthUser {
    username: string;
    password: string;
}

export default IAuthUser;
