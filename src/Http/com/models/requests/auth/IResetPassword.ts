interface IResetPassword {
    new_password: string;
}

export default IResetPassword;
