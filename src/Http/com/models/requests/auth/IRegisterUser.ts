interface IRegisterUser {
    first_name: string;
    other_names: string;
    email: string;
    password: string;
    gender: string;
    age: number;
    variant: number;
}

export default IRegisterUser;
