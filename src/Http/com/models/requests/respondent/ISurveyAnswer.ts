interface ISurveyAnswer {
    question: number;
    option: number;
}

export default ISurveyAnswer;
