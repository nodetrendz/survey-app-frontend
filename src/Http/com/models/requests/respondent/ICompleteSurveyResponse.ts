interface ICompleteSurveyResponse {
    status: boolean;
    message: string;
}

export default ICompleteSurveyResponse;
