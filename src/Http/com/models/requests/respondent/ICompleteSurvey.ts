import ISurveyAnswer from "./ISurveyAnswer";

interface ICompleteSurvey {
    survey: number;
    answers: Array<ISurveyAnswer>;
}

export default ICompleteSurvey;
