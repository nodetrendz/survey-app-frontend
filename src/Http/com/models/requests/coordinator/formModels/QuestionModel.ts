import OptionModel from "./OptionModel";

class QuestionModel {
    id?: number;
    text: string;
    options: Array<OptionModel>;

    constructor(title: string = "") {
        this.text = title;
        this.options = [];
    }
}

export default QuestionModel;
