import IDefineSurveyQuestion from "./IDefineSurveyQuestion";

interface IDefineSurvey {
    title: string;
    questions: Array<IDefineSurveyQuestion>;
}

export default IDefineSurvey;
