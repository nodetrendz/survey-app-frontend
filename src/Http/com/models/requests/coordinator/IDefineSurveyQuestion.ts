interface IDefineSurveyQuestion {
    text: string;
    options: Array<string>;
}

export default IDefineSurveyQuestion;
