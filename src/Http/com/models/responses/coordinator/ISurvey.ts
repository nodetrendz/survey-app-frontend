interface ISurvey {
    id: number;
    survey_name: string;
    date_created: string;
    date_modified: string;
    questions: number;
    responses: number;
    status: string;
}

export default ISurvey;
