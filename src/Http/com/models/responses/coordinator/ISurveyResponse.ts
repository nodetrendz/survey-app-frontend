import ISurvey from "./ISurvey";

interface ISurveyResponse {
    status: boolean;
    survey: ISurvey;
}
export default ISurveyResponse;
