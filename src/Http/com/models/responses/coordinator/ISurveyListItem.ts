interface ISurveyListItem {
    id: number;
    created: string;
    status: string;
    title: string;
}

export default ISurveyListItem;