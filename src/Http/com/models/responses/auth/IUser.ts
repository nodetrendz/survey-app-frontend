import IUserVariant from "./IUserVariant";

interface IUser {
    id: number;
    first_name: string;
    other_names: string;
    gender: string;
    age: number;
    email: string;
    variant: IUserVariant;
}

export default IUser;
