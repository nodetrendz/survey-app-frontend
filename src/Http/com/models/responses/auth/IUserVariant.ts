interface IUserVariant {
    id: number;
    name: string;
}
export default IUserVariant;
