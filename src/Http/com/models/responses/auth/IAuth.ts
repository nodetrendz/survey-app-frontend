interface IAuth {
    status: boolean;
    token: string;
    message?: string;
}

export default IAuth;
