import ISurveyContext from "Http/com/context/ISurveyContext";
import SurveyContext from "Http/com/context/SurveyContext";
import AuthService from "Http/Service/AuthService";
import React, { Fragment, useContext, useEffect } from "react";
import { useHistory } from "react-router";

const LogoutComponent: React.FC = () => {

    const history = useHistory();
    const context: ISurveyContext = useContext(SurveyContext);

    const handleRequest = async () => {
        const req = await AuthService.logout();
        handleNotification(req);
    };

    
    const handleNotification = (authenticated: any) => {
        // set context values
        
        context.notification.status = authenticated.status;
        context.notification.text = authenticated.message;

        return setTimeout(() => {
            
            // clear notification
            
            context.notification.status = undefined;
            context.notification.text = undefined;
            
        }, 1000);

    };

    useEffect(() => {

        let mounted = true;

        if (mounted) {
            handleRequest();
            history.push('/signin');
        }

        return () => {
            mounted = false;
        };

    }, []);

    return <Fragment />
};

export default LogoutComponent;