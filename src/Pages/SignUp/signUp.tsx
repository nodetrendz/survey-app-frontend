import Button from "Components/Button/button";
import backgroundImg from "Assets/images/background1.png";
import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import styles from "./signUp.module.scss";
import AuthService from "Http/Service/AuthService";
import IRegisterUser from "Http/com/models/requests/auth/IRegisterUser";
import Icon from "Components/Icon/icon";

const SignUp: React.FC = () => {
  const history = useHistory();

  const [firstname, setFirstname] = useState<string>("");
  const [othernames, setOthernames] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [gender, setGender] = useState<string>("o");
  const [variant, setVariant] = useState<number>(2);
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);

  const submitEventHandler = async (submitEvent: any) => {
    // submitEvent.preventDefault();
    setIsSubmitting(true);
    const user: IRegisterUser = {
      first_name: firstname,
      other_names: othernames,
      email: email,
      password: password,
      gender: gender,
      age: 0,
      variant: variant,
    };

    const response = await AuthService.register_user(user);

    if (response) history.push("/signin");
    setIsSubmitting(false);
  };

  return (
    <div className={styles.mainWrapper}>
      <div className={styles.wrapper}>
        <div className={styles.signUp}>
          <div className={styles.signUp__logo}>
            <Icon name="logo" />
          </div>
          <div className={styles.signUp__header}>
            <h1>Get started for free</h1>
            <p>Enter your details to get started</p>
          </div>
          <div className={styles.signUp__form}>
            <form>
              <div className={styles.signUp__form_input}>
                <label htmlFor="firstname">first name</label>
                <input
                  type="text"
                  name="firstname"
                  value={firstname}
                  onChange={(e: any) => setFirstname(e.target.value)}
                />
              </div>
              <div className={styles.signUp__form_input}>
                <label htmlFor="othernames">other names</label>
                <input
                  type="text"
                  name="othernames"
                  value={othernames}
                  onChange={(e: any) => setOthernames(e.target.value)}
                  readOnly={isSubmitting}
                />
              </div>
              <div className={styles.signUp__form_input}>
                <label htmlFor="email">email address</label>
                <input
                  type="email"
                  name="email"
                  value={email}
                  onChange={(e: any) => setEmail(e.target.value)}
                  readOnly={isSubmitting}
                />
              </div>
              <div className={styles.signUp__form_input}>
                <label htmlFor="password">password</label>
                <input
                  type="password"
                  name="password"
                  value={password}
                  onChange={(e: any) => setPassword(e.target.value)}
                  readOnly={isSubmitting}
                />
              </div>
              <div className={styles.signUp__form_input}>
                <label>
                  <input
                    type="radio"
                    name="variant"
                    value={2}
                    onChange={(e: any) => setVariant(e.target.value)}
                    readOnly={isSubmitting}
                    defaultChecked={true}
                  />
                  I am a Respondent
                </label>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <label>
                  <input
                    type="radio"
                    name="variant"
                    value={1}
                    onChange={(e: any) => setVariant(e.target.value)}
                    readOnly={isSubmitting}
                  />
                  I am a Coordinator
                </label>
              </div>
              <div className={styles.signUp__form_button}>
                <Button
                  title="Create account"
                  isSubmitting={isSubmitting}
                  handleClick={submitEventHandler}
                />
              </div>
            </form>
          </div>
          <p className={styles.signIn__prompt}>
            Already have an account? <Link to="/signin">Sign In</Link>
          </p>
        </div>
      </div>
      <div className={styles.right}>
        <img src={backgroundImg} alt="blue patterned Img" />
      </div>
    </div>
  );
};
export default SignUp;
