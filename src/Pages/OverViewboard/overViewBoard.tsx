import React, { Fragment, useContext, useEffect, useState } from "react";
import styles from "./overViewBoard.module.scss";
import Button from "Components/Button/button";
import Modal from "Components/Modal/modal";
import SurveyList from "Components/SurveyListAdmin/surveyListAdmin";
import UserLayout from "Components/UserLayout/userLayout";
import CoordinatorService from "Http/Service/CoordinatorService";
import SurveyInfoCards from "Components/SurveyInfoCards/surveyInfoCards";
import ISurveyContext from "Http/com/context/ISurveyContext";
import SurveyContext from "Http/com/context/SurveyContext";
import { useHistory } from "react-router";
import Icon from "Components/Icon/icon";
import { isNotEmptyArray } from "Libs/Utils/utils";

const OverViewboard: React.FC = () => {
  const [surveys, setSurveys] = useState<Array<any>>([]);
  const fetchSurveys = async () =>
    setSurveys(await CoordinatorService.fetch_my_surveys());

  useEffect(() => {
    let mounted = true;

    mounted && fetchSurveys();

    return () => {
      mounted = false;
    };
  }, []);
  const [close, setClose] = useState(true);

  return (
    <Fragment>
      <UserLayout title="Overview">
        <div className={styles.overViewboard}>
          <div className={styles.overViewboard__buttonWrapper}>
            <div className={styles.overViewboard__button_createSurvey}>
              <Button
                title="Create Survey"
                icon="plus"
                handleClick={() => setClose(false)}
              />
            </div>
          </div>
          <div className={styles.overViewboard__surveyInfo}>
            <SurveyInfoCards />
          </div>
          <div className={styles.overViewboard__mainContent}>
            {surveys && isNotEmptyArray(surveys) ? (
              <div className={styles.surveyList}>
                <SurveyList surveys={surveys} setSurveys={setSurveys} />
              </div>
            ) : (
              <div className={styles.noSurvey}>
                <div className={styles.noSurvey__info}>
                  <Icon name="document" />
                  <p>
                    You have not created any survey. Use the the create button
                    to get started
                  </p>
                </div>
              </div>
            )}
            <div className={styles.modalWrapper}>
              <Modal close={close} setClose={setClose} />
            </div>
          </div>
        </div>
      </UserLayout>
    </Fragment>
  );
};
export default OverViewboard;
