import React, { Fragment, useContext, useEffect, useState } from "react";
import Button from "Components/Button/button";
import ResponderQuestion from "Components/ResponderQuestion/responderQuestion";
import UserLayout from "Components/UserLayout/userLayout";
import styles from "./responderSurveyQuestion.module.scss";
import RespondentService from "Http/Service/RespondentService";
import { useHistory, useParams } from "react-router";
import SurveyContext from "Http/com/context/SurveyContext";
import ISurveyContext from "Http/com/context/ISurveyContext";
import ICompleteSurveyResponse from "Http/com/models/requests/respondent/ICompleteSurveyResponse";

const ResponderSurveyQuestion = () => {

  const { id }: any = useParams();
  const history = useHistory();

  const context: ISurveyContext = useContext(SurveyContext);

  const [exam, setExam] = useState<any>();
  const [title, setTitle] = useState<any>();
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);

  useEffect(() => {

    let mounted = true;

    if (mounted ) {
      // check if user can write exam
      RespondentService.can_take_survey(id).then(({status, message}: any) => {
        if (status && status == true) {
          notifySuccess(message);
        } else {
          history.push('/responder');
          notifyError(message);
        }
      }).catch((err: any) => history.push('/responder'));
      RespondentService.get_open_survey_by_id(id).then((exam: any) => {
        setTitle(exam.title);
        setExam(exam);
      });
    }

    return () => {
      mounted = false;
      context.answers = [];
    };

  }, []);

  const handleQuestionChangeEvent = (question: number, option: number) => {
    context.answers[question] = option;
  };

  const notifyUser = (status: boolean, message: string) => {
    // set context values
            
    context.notification.status = status;
    context.notification.text = message;

    return setTimeout(() => {
        
        // clear notification
        
        context.notification.status = undefined;
        context.notification.text = undefined;
        
    }, 1000);
  };

  const notifySuccess = (message: string) => {
    notifyUser(true, message);
  }

  const notifyError = (message: string) => {
    notifyUser(false, message);
  }

  const handleSubmitEvent = async (submitEvent: any) => {
    submitEvent.preventDefault();
    setIsSubmitting(true);
    const request: any = {
      survey: id,
      answers: []
    };

    for(const [key, value] of Object.entries(context.answers)) {
      request.answers = [...request.answers, { question: key, option: value}];
    }

    const response: ICompleteSurveyResponse | undefined = await RespondentService.complete_survey(request);
    setIsSubmitting(false);
    if (response && response.status) {
      notifySuccess(response.message);
      history.push('/responder');
    } else {
      response && notifyError(response.message);
    }
  };

  return (
    <Fragment>
      <UserLayout title={title}>
        <form method="post" onSubmit={handleSubmitEvent}>
        <div className={styles.responderSurveyQuestion}>
          {
            exam && exam.questions && exam.questions.map((question: any, key: number) => <ResponderQuestion key={key} index={key + 1} question={question} onChange={handleQuestionChangeEvent} />)
          }
          <div className={styles.responderSurveyQuestion__bottom}>
            <div className={styles.buttons__wrapper}>
              <div className={styles.button_cancel}>
                <Button title="Cancel" handleClick={(e: any) => history.push('/responder')} />
              </div>
              <div className={styles.button_submit}>
                <Button title="Submit" type={"submit"} isSubmitting={isSubmitting} />
              </div>
            </div>
          </div>
        </div>
        </form>
      </UserLayout>
    </Fragment>
  );
};

export default ResponderSurveyQuestion;
