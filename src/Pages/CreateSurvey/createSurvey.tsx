import AddQuestion from "Components/AddQuestion/addQuestion";
import UserLayout from "Components/UserLayout/userLayout";
import ISurveyContext from "Http/com/context/ISurveyContext";
import SurveyContext from "Http/com/context/SurveyContext";
import React, { Fragment, useContext, useEffect } from "react";
import styles from "./createSurvey.module.scss";


const CreateSurvey: React.FC = () => {

  const context: ISurveyContext = useContext(SurveyContext);

  const manageSurveyName = () => {
    const currentTime = (Math.random() + 1).toString(36).substring(2) + "-" + Date.now();
    context.title = context.title || `Template Survey (${currentTime})`;
  };

  useEffect(() => {

    let mounted = true;

    if (mounted) {
      manageSurveyName();
    }

    return () => {
      mounted = false;
    };

  }, []);

  return (
    <Fragment>
      <UserLayout title={context.title}>
        <div className={styles.navigation}>
          <div className={styles.navigation__main}>
            <AddQuestion />
          </div>
        </div>
      </UserLayout>
    </Fragment>
  );
};

export default CreateSurvey;
