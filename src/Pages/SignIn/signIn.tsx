import React, { useContext, useState } from "react";
import Button from "Components/Button/button";
import backgroundImg from "Assets/images/background1.png";
import styles from "./signIn.module.scss";
import { Link, Redirect, useHistory } from "react-router-dom";
import AuthService from "Http/Service/AuthService";
import ISurveyContext from "Http/com/context/ISurveyContext";
import SurveyContext from "Http/com/context/SurveyContext";
import Icon from "Components/Icon/icon";

const SignIn: React.FC = () => {
  const history = useHistory();
  const context: ISurveyContext = useContext(SurveyContext);

  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [error, setError] = useState<string>("");
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);

  const handleAuthentication = async (event: any) => {
    setIsSubmitting(true);
    event.preventDefault();

    const authenticated = await AuthService.authenticate_user({
      username,
      password,
    });

    if (authenticated.status === true) {
      // fetch the user and set in context
      context.user = await AuthService.fetch_me();

      setIsSubmitting(false);
      handleNotification(authenticated);

      if (context.user?.variant && context.user?.variant.id === 1) {
        history.push("/");
      } else {
        history.push("/responder");
      }
    } else {
      setIsSubmitting(false);
      setError(authenticated.message || "");
      handleNotification(authenticated);
    }
  };

  const handleNotification = (authenticated: any) => {
    // set context values
    context.notification.status = authenticated.status;
    context.notification.text = authenticated.message;

    return setTimeout(() => {
      // clear notification
      context.notification.status = undefined;
      context.notification.text = undefined;
    }, 1000);
  };

  return (
    <div className={styles.mainWrapper}>
      <div className={styles.wrapper}>
        <div className={styles.signIn}>
          <div className={styles.signIn__logo}>
            <Icon name="logo" />
          </div>
          <div className={styles.signIn__header}>
            <h1>Sign In</h1>
            {error ? (
              <small className={styles.signIn__header__error}>{error}</small>
            ) : (
              <p>Please enter your credentials to proceed.</p>
            )}
          </div>
          <div className={styles.signIn__form}>
            <form>
              <div className={styles.signIn__form_input}>
                <label htmlFor="email">email address</label>
                <input
                  type="email"
                  name="email"
                  value={username}
                  onChange={(e: any) => setUsername(e.target.value)}
                />
              </div>
              <div className={styles.signIn__form_input}>
                <div className={styles.label__heading}>
                  <label htmlFor="password">password</label>
                  <Link to="#">Forgot Password ?</Link>
                </div>

                <input
                  type="password"
                  name="password"
                  value={password}
                  onChange={(e: any) => setPassword(e.target.value)}
                />
              </div>
              <div className={styles.signIn__form_button}>
                <Button
                  title="Sign In"
                  isSubmitting={isSubmitting}
                  handleClick={handleAuthentication}
                />
              </div>
            </form>
          </div>
          <p className={styles.signUp__prompt}>
            Don't have an account? <Link to="/signup">Sign up</Link>
          </p>
        </div>
      </div>
      <div className={styles.right}>
        <img src={backgroundImg} alt="blue patterned Img" />
      </div>
    </div>
  );
};
export default SignIn;
