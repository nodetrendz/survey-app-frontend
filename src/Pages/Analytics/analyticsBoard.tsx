import React, {Fragment, useContext, useEffect, useState} from "react";
import styles from "./analyticsBoard.module.scss";
import Button from "Components/Button/button";
import AnalyticsCard from "Components/Analytics/analyticsCard";
import UserLayout from "Components/UserLayout/userLayout";
import {Link, useHistory, useParams} from "react-router-dom";
import AnalyticsService from "../../Http/Service/AnalyticsService";
import IAnalyticsContext from "../../Http/com/context/IAnalyticsContext";
import AnalyticsContext from "../../Http/com/context/AnalyticsContext";
import Spinner from "../../Components/Spinner/spinner";

const AnalyticsBoard: React.FC = () => {
  const { id }: any = useParams();
  const history = useHistory();
  const context: IAnalyticsContext = useContext(AnalyticsContext);

  const [place, setPlace] = useState(<Spinner />);

  useEffect(() => {
      let mounted = true;

      if (mounted)
      {
        AnalyticsService.get_survey(id).then(({status, data, message}: any) => {
          if (status && status == true) {
            context.tableData = data.tableData;
            context.pieData = data.pieData;
            context.barData = data.barData;
            setPlace( <AnalyticsCard />);
          } else {
            notifyError(message);
            history.push('/');
          }
        }).catch((err: any) => {
          history.push('/');
          notifyError('Something went wrong. Please try again.');
        });
      }

      return () => {
        mounted = false;
      };

  }, []);


  const notifyUser = (status: boolean, message: string) => {
    // set context values

    context.notification.status = status;
    context.notification.text = message;

    return setTimeout(() => {

      // clear notification

      context.notification.status = undefined;
      context.notification.text = undefined;

    }, 1000);
  };

  const notifyError = (message: string) => {
    notifyUser(false, message);
  };

  return (
    <Fragment>
      <UserLayout title="Survey Report">
        <div className={styles.analyticsBoard}>
          <div className={styles.analyticsBoard__buttonWrapper}>
            <div className={styles.analyticsBoard__button_backButton}>
              <Link to="/">
                <Button title="Go Back" icon="arrow-left" />
              </Link>
            </div>
          </div>
          { place }
        </div>
      </UserLayout>
    </Fragment>
  );
};
export default AnalyticsBoard;
