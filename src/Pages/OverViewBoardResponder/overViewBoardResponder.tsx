import React, { Fragment } from "react";
import SurveyListResponder from "Components/SurveyListResponder/surveyListResponder";
import UserLayout from "Components/UserLayout/userLayout";
import styles from "./overViewBoradResponder.module.scss";


const OverViewboardResponder: React.FC = () => {

  return (
    <Fragment>
      <UserLayout title="Survey">
        <div className={styles.overViewboardResponder}>
          <SurveyListResponder />
        </div>
      </UserLayout>
    </Fragment>
  );
};
export default OverViewboardResponder;
