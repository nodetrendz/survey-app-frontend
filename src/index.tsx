import React from "react";
import ReactDOM from "react-dom";
import Routes from "Routes/routes";
import { BrowserRouter } from "react-router-dom";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import SurveyContext, { question_models } from "Http/com/context/SurveyContext";
import AnalyticsContext, { data_models } from "Http/com/context/AnalyticsContext";
import NotificationProvider from "Http/com/context/NotificationContext";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <NotificationProvider>
        <SurveyContext.Provider value={question_models}>
            <AnalyticsContext.Provider value={data_models}>
                <Routes />
            </AnalyticsContext.Provider>
        </SurveyContext.Provider>
      </NotificationProvider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
