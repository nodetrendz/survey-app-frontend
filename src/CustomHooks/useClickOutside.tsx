import { useRef, useEffect } from "react";

const useClickOutside = (handler: Function, id: any) => {
  const domNode = useRef<any>();

  useEffect(() => {
    const maybeHandler = (event: any) => {
      if (
        domNode.current &&
        domNode.current.contains(event.target) &&
        domNode.current.id === id.toString()
      ) {
        handler(true);
        console.log(id, "id");
      }
      if (domNode.current && !domNode.current.contains(event.target)) {
        handler(false);
      }
    };
    document.addEventListener("mousedown", maybeHandler);

    return () => {
      document.removeEventListener("mousedown", maybeHandler);
    };
  });

  return domNode;
};

export default useClickOutside;
