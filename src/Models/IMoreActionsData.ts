interface IMoreActionsData {
    title: string, 
    icon: string, 
    handleClick?: any, 
    route?: any
}

export default IMoreActionsData;