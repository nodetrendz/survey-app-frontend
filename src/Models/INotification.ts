interface INotification {
    text?: string;
    status?: boolean;
}

export default INotification;