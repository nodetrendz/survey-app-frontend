import React, { Fragment, useEffect, useState, useContext } from "react";
import Navigation from "Components/Navigation/navigation";
import INotifcationContext from "Http/com/context/INotificationContext";
import { NotificationContext } from "Http/com/context/NotificationContext";
import Notification from "Components/Notification/notification";
import IUser from "Http/com/models/responses/auth/IUser";
import AuthService from "Http/Service/AuthService";
import styles from "./userLayout.module.scss";
// import {
//   CountProvider,
//   CountContext,
// } from "Http/com/context/NotificationContext";

import { composeClasses } from "Libs/Utils/utils";
interface IProps {
  children: React.ReactNode;
  title: string;
}
const defaultProps: IProps = {
  children: " ",
  title: " ",
};

const UserLayout: React.FunctionComponent<IProps> = (props: IProps) => {
  const { state, dispatch } = React.useContext(NotificationContext);

  const [success, setSuccess] = useState<boolean | undefined>(undefined);
  const [error, setError] = useState<boolean | undefined>(undefined);

  useEffect(() => {
    let mounted = true;

    if (mounted && state) {
      setSuccess(state.success);
      setError(state.error);
    }
    return () => {
      mounted = false;
    };
  }, [state]);

  const [username, setUsername] = useState<string>("");
  const [variantName, setvariantName] = useState<string>("");

  const handleUserSetting = async () => {
    const user: IUser | undefined = await AuthService.fetch_me();
    setvariantName(`${user?.variant.name}`);
    setUsername(`${user?.first_name} ${user?.other_names}`);
  };

  useEffect(() => {
    let mounted = true;

    if (mounted) {
      // get the user
      handleUserSetting();
    }

    return () => {
      mounted = false;
    };
  }, []);

  return (
    <Fragment>
      <Navigation
        title={props.title}
        username={username}
        variantName={variantName}
      />
      <div className={styles.mainContent}>
        {props.children}
        <div
          className={composeClasses(
            styles.notification__wrapper,
            success || error ? styles.showNotification : styles.hideNotification
          )}
        >
          <Notification />
        </div>
      </div>
    </Fragment>
  );
};
UserLayout.defaultProps = defaultProps;

export default UserLayout;
