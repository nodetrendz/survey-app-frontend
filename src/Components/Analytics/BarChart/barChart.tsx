import styles from "./barChart.module.scss";
import React, {useContext} from "react";
import IAnalyticsContext from "../../../Http/com/context/IAnalyticsContext";
import AnalyticsContext from "../../../Http/com/context/AnalyticsContext";
import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
} from "recharts";

const COLORS = ["#8884d8", "#82ca9d", "#0088FE", "#FFBB28", "#FF8042"];


const App: React.FC = () =>  {
    const context: IAnalyticsContext = useContext(AnalyticsContext);
    const data = context.barData;
    return (
        <div className={styles.main}>
            <BarChart
                width={800}
                height={300}
                data={data}
                margin={{
                    top: 20,
                    right: 30,
                    left: 20,
                    bottom: 5
                }}
            >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Legend />

                {
                    Object.entries(data[0]).map(([key, value], index: number) => {
                        if (key !== "name") {
                            return <Bar dataKey={key} stackId="a" fill={COLORS[index-1]} />;
                        }
                    })
                }
            </BarChart>
        </div>
    );
};

export default App;