export const analyticsInfoData = [
  {
    title: "Question 1",
    options: {
        option1: 5,
        option2: 4,
        option3: 5,
        option4: 8,
        option5: 5,
    },
  },
  {
    title: "Question 2",
     options: {
        option1: 3,
        option2: 5,
        option3: 6,
        option4: 5,
        option5: 9,
    },
  },
  {
    title: "Question 3",
    options: {
        option1: 5,
        option2: 2,
        option3: 9,
        option4: 1,
        option5: 7,
    },
  },
  {
    title: "Question 4",
    options: {
        option1: 5,
        option2: 5,
        option3: 5,
        option4: 5,
        option5: 5,
    },
  },
  {
    title: "Question 5",
    options: {
        option1: 5,
        option2: 5,
        option3: 5,
        option4: 5,
        option5: 5,
    },
  },
  {
    title: "Question 6",
    options: {
        option1: 5,
        option2: 5,
        option3: 5,
        option4: 5,
        option5: 5,
    },
  },
  {
    title: "Question 7",
    options: {
        option1: 5,
        option2: 5,
        option3: 5,
        option4: 5,
        option5: 5,
    },
  },
  {
    title: "Question 8",
    options: {
        option1: 5,
        option2: 5,
        option3: 5,
        option4: 5,
        option5: 5,
    },
  },
  {
    title: "Question 9",
    options: {
        option1: 5,
        option2: 5,
        option3: 5,
        option4: 5,
        option5: 5,
    },
  },
  {
    title: "Question 10",
    options: {
        option1: 5,
        option2: 5,
        option3: 5,
        option4: 5,
        option5: 5,
    },
   },
];