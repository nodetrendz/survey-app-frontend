import React, {useContext} from "react";
import styles from "./tableData.module.scss";
import IAnalyticsContext from "../../../Http/com/context/IAnalyticsContext";
import AnalyticsContext from "../../../Http/com/context/AnalyticsContext";

const analyticsInfo = (data: any) => {
    return data.map((e: any, i: number) => {
        return (
            <div className={styles.tableData} key={i}>
                <div className={styles.tableData_question}>
                    <h1>{e.title}</h1>
                </div>
                <div className={styles.tableData__count}>
                    <p className={styles.open}>{e.options.option1}</p>
                </div>
                <div className={styles.tableData__count}>
                    <p className={styles.open}>{e.options.option2}</p>
                </div>
                <div className={styles.tableData__count}>
                    <p className={styles.open}>{e.options.option3}</p>
                </div>
                <div className={styles.tableData__count}>
                    <p className={styles.open}>{e.options.option4}</p>
                </div>
                <div className={styles.tableData__count}>
                    <p className={styles.open}>{e.options.option5}</p>
                </div>
            </div>
        );
    });
};
const TableData = () => {
    const context: IAnalyticsContext = useContext(AnalyticsContext);
    const data = context.tableData;
  return (
      <div className={styles.tableData__main}>
          <div className={styles.header}>
              <p className={styles.header__option1}></p>
              <p className={styles.header__option}>Option1</p>
              <p className={styles.header__option}>Option2</p>
              <p className={styles.header__option}>Option2</p>
              <p className={styles.header__option}>Option4</p>
              <p className={styles.header__option}>Option5</p>
          </div>
          <div className={styles.tableData__main_list}>
              {analyticsInfo(data)}
          </div>
      </div>
  );
};

export default TableData;
