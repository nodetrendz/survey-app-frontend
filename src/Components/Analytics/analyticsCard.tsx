import TableData from "Components/Analytics/TableData/tableData";
import BarGraph from "Components/Analytics/BarChart/barChart";
import PieGraph from "Components/Analytics/PieChart/pieChart";
import React, {Fragment, useState} from "react";
import styles from "./analyticsCard.module.scss";


const AnalyticsCard: React.FC = () => {

    const [item, setItem] = useState<string>("tableData");

    return (
        <div className={styles.analyticsCard}>
            <div className={styles.analyticsCard__heading}>
                <h1>Survey Responses</h1>
                <div className={styles.analyticsCard__select}>
                    <select onChange={event => setItem(event.target.value)} placeholder={"View Stats"}>
                        <option value={"tableData"}>Tabular Data</option>
                        <option value={"pieChart"}>Pie Chat</option>
                        <option value={"barChart"}>Bar Graph</option>
                    </select>
                </div>
            </div>
            {
                item === "tableData" ? <TableData /> : <Fragment />
            }
            {
                item === "barChart" ? <BarGraph /> : <Fragment />
            }
            {
                item === "pieChart" ? <PieGraph /> : <Fragment />
            }
        </div>
    );
};

export default AnalyticsCard;
