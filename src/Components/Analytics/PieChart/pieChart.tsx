import styles from "./pieChart.module.scss";
import { PieChart, Pie, Cell } from "recharts";
import IAnalyticsContext from "../../../Http/com/context/IAnalyticsContext";
import AnalyticsContext from "../../../Http/com/context/AnalyticsContext";
import React, {useContext} from "react";

const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
   from,
   cx,
   cy,
   midAngle,
   innerRadius,
   outerRadius,
   percent,
   index
}: any) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + (radius - 25) * Math.cos(-midAngle * RADIAN);
    const y = cy + (radius) * Math.sin(-midAngle * RADIAN);

    return (
        <text
            x={x}
            y={y}
            fill="white"
            textAnchor={x > cx ? "start" : "end"}
            dominantBaseline="central"
        >
            {from[index].name}{` ${(percent * 100).toFixed(0)}%`}
        </text>
    );
};
const App: React.FC = () => {
    const context: IAnalyticsContext = useContext(AnalyticsContext);
    const data = context.pieData;
    return (
        <div className={styles.main}>
            <PieChart width={300} height={300}>
                <Pie
                    data={data}
                    cx={145}
                    cy={145}
                    labelLine={false}
                    label={renderCustomizedLabel}
                    outerRadius={150}
                    from={data}
                    fill="#8884d8"
                    dataKey="value"
                >
                    {data.map((entry: any, index: any) => (
                        <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                    ))}
                </Pie>
            </PieChart>
        </div>
    );
};

export default App;