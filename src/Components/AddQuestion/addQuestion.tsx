import Button from "Components/Button/button";
import Question from "Components/Question/question";
import ISurveyContext from "Http/com/context/ISurveyContext";
import SurveyContext from "Http/com/context/SurveyContext";
import OptionModel from "Http/com/models/requests/coordinator/formModels/OptionModel";
import QuestionModel from "Http/com/models/requests/coordinator/formModels/QuestionModel";
import IDefineSurvey from "Http/com/models/requests/coordinator/IDefineSurvey";
import CoordinatorService from "Http/Service/CoordinatorService";
import React, { Fragment, useContext, useEffect, useState } from "react";
import { useHistory } from "react-router";
import styles from "./addQuestion.module.scss";

const QUESTIONS_LIMIT = 10;


const AddQuestion: React.FC = () => {

  const context: ISurveyContext = useContext(SurveyContext);

  const history = useHistory();

  const [questions, setQuestions] = useState<Array<any>>([]);
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const [questionModels, setQuestionModels] = useState<Array<QuestionModel>>([]);

  const [isAddingNewQuestion, setIsAddingNewQuestion] = useState<boolean>(false);
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);

  const appendQuestion = () => {
    const tempQuestions = [...questions];
    const tempCurrentQuestionModels = [...questionModels];

    if (currentIndex >= QUESTIONS_LIMIT) return;

    if (currentIndex > 0) tempQuestions.push(<hr />);

    context.questionModels[currentIndex] = tempCurrentQuestionModels[currentIndex] = new QuestionModel("");
    tempQuestions.push(<Question key={Date.now()} index={currentIndex + 1} onChange={handleQuestionChangeEvent} />);

    setQuestionModels(tempCurrentQuestionModels);
    setQuestions([...tempQuestions]);
    setCurrentIndex(currentIndex + 1);
  };

  const handleQuestionChangeEvent = (question: QuestionModel, index: number) => {
    index -= 1;
    const tempCurrentQuestionModels = [...questionModels];
    tempCurrentQuestionModels[index] = {...question};

    console.log(tempCurrentQuestionModels);
    setQuestionModels([...tempCurrentQuestionModels]);
  };

  const handleAddNewQuestionClickEvent = (clickEvent: any) => {
    clickEvent.preventDefault();
    setIsAddingNewQuestion(true);
    appendQuestion();
    setIsAddingNewQuestion(false);
  };

  const handleSubmitEvent = async (submitEvent: any) => {
    submitEvent.preventDefault();
    setIsSubmitting(true);

    const payload: IDefineSurvey = {
      title: context.title,
      questions: context.questionModels.map((model: QuestionModel) => {
        return {
          text: model.text,
          options: model.options.map((option: OptionModel) => option.value)
        };
      })
    };

    const response = await CoordinatorService.define_survey(payload);
    setIsSubmitting(false);
    if (response && response.status) {
      history.push('/');
    }
  };

  

  useEffect(() => {
    let mounted = true;

    mounted && appendQuestion();

    return () => {
      mounted = false;
    };
  }, []);

  return (
      <Fragment>
        <form onSubmit={handleSubmitEvent} method={"post"}>
          <div className={styles.wrapper}>
            <div className={styles.addQuestion}>
              <div className={styles.addQuestion__top}>
                <div className={styles.addQuestion__button_newQuestion}>
                  <Button title="Add new question" icon="plus" handleClick={handleAddNewQuestionClickEvent} isSubmitting={isAddingNewQuestion} />
                </div>
              </div>
              <div className={styles.addQuestion__panels}>
                {questions}
              </div>
            </div>
            <div className={styles.addQuestion__bottom}>
              <div className={styles.button__wrapper}>
                <div className={styles.button_cancel}>
                  <Button title="Cancel" handleClick={(e: any) => history.push('/')} />
                </div>
                <div className={styles.button_save}>
                  <Button title="Save" type={"submit"} isSubmitting={isSubmitting} />
                </div>
              </div>
            </div>
          </div>
        </form>
      </Fragment>
  );
};

export default AddQuestion;
