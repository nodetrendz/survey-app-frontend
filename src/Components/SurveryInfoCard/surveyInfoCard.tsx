import { composeClasses } from "Libs/Utils/utils";
import React from "react";
import styles from "./surveyInfoCard.module.scss";

export interface IProps {
  title: string;
  count: number;
  percentage: any;
  open: boolean;
}
const SurveyInfoCard: React.FunctionComponent<IProps> = (props: IProps) => {
  return (
    <div className={styles.surveryInfoCard}>
      <h1 className={styles.surveryInfoCard__title}>{props.title}</h1>
      <div className={styles.surveryInfoCard__bottom}>
        <p className={styles.count}>{props.count}</p>
        <div
          className={composeClasses(
            styles.percentage,
            props.open ? styles.open : styles.closed
          )}
        >
          <p>{props.percentage}%</p>
        </div>
      </div>
    </div>
  );
};

export default SurveyInfoCard;
