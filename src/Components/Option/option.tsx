import ISurveyContext from "Http/com/context/ISurveyContext";
import SurveyContext from "Http/com/context/SurveyContext";
import React, { useContext, useEffect, useState } from "react";
import styles from "./option.module.scss";

interface IOption {
  index: number;
  parentIndex: number;

  handleChange: Function;
}

const Option: React.FC<IOption> = (props: IOption) => {

  const context: ISurveyContext = useContext(SurveyContext);

  const [placeholder, setPlaceholder] = useState<string>(`Define choice option ${props.index}`);
  const [text, setText] = useState<string>("");

  useEffect(() => {
    let mounted = true;

    mounted && props && props.index && setPlaceholder(`Define choice option ${props.index}`);

    return () => {
      mounted = false;
    };
  }, []);


  useEffect(() => {
    let mounted = true;

    if (mounted && props && props.index && props.parentIndex) {
      context.questionModels[props.parentIndex - 1].options[props.index - 1].value = text;
      props.handleChange(context.questionModels[props.parentIndex - 1].options[props.index - 1], props.index);
    }

    return () => {
      mounted = false;
    };
  }, [text]);


  return (
    <div className={styles.option}>
      <input type="checkbox" className={styles.option__checkbox} readOnly />
      <input
        type="text"
        name={`option_${props.parentIndex}_${props.index}`}
        placeholder={placeholder}
        className={styles.option__text}
        required={true}
        value={text || ''}
        onChange={(e: any) => setText(e.target.value)}
      />
    </div>
  );
};

Option.defaultProps = {
  index: 1
};

export default Option;
