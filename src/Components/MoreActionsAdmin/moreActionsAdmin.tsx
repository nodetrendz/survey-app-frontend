import React, { Fragment } from "react";
import { isNotEmptyArray } from "Libs/Utils/utils";
import { moreActionsData } from "./data";
import styles from "./moreActionsAdmin.module.scss";
import Icon from "Components/Icon/icon";
import { Link } from "react-router-dom";
import IMoreActionsData from "Models/IMoreActionsData";

interface IMoreActionsAdmin {
  moreActionsData: Array<IMoreActionsData>;
}

const MoreActionsAdmin: React.FC<IMoreActionsAdmin> = (props: IMoreActionsAdmin) => {

  const manageLink = (prop: IMoreActionsData, index: number) => {
    let element = <Fragment />;

    if (prop.route) {
        element = <Link to={prop.route} key={index}>
          <div className={styles.moreActions__option}>
            <Icon name={prop.icon} />
            <p>{prop.title}</p>
          </div>
        </Link>;
    } else {
      element = <div className={styles.moreActions__option} onClick={prop.handleClick} key={index}>
        <Icon name={prop.icon} />
        <p>{prop.title}</p>
      </div>;
    }

    return element;
  }

  return (
    <div className={styles.moreActions}>
      {props && props.moreActionsData &&
        isNotEmptyArray(moreActionsData) &&
        props && props.moreActionsData.map((e, i: number) => manageLink(e, i))}
    </div>
  );
};

export default MoreActionsAdmin;
