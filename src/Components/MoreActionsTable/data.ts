export const moreActionsData: Array<{title: string, icon: string}> = [
  {
    title: "Table Data",
    icon: "table",
  },
    {
    title: "Pie Chart",
     icon: "pieChart",
  },
  {
    title: "Bar Chart",
    icon: "barChart",
  },
];