import Icon from "Components/Icon/icon";
import React from "react";
import { isNotEmptyArray } from "Libs/Utils/utils";
import { moreActionsData } from "./data";
import styles from "./moreActionsTable.module.scss";
const MoreActions = () => {
  return (
    <div className={styles.moreActions}>
      {moreActionsData &&
        isNotEmptyArray(moreActionsData) &&
        moreActionsData.map((e, i) => {
          return (
            <div className={styles.moreActions__option}>
              <Icon name={e.icon} />
              <p>{e.title}</p>
            </div>
          );
        })}
    </div>
  );
};

export default MoreActions;
