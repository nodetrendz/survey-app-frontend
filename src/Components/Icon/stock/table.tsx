/* eslint-disable max-len */
import React from "react";
/**
 * Account Profile Icon
 * @param {Object} props Component props
 * @returns {React.Component} React component
 */
const SVG = (props: unknown): unknown => (
  <svg
    width="13"
    height="12"
    viewBox="0 0 13 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M11.3333 0H1.33333C0.6 0 0 0.6 0 1.33333V10.6667C0 11.4 0.6 12 1.33333 12H11.3333C12.0667 12 12.6667 11.4 12.6667 10.6667V1.33333C12.6667 0.6 12.0667 0 11.3333 0ZM11.3333 1.33333V3.33333H1.33333V1.33333H11.3333ZM8 10.6667H4.66667V4.66667H8V10.6667ZM1.33333 4.66667H3.33333V10.6667H1.33333V4.66667ZM9.33333 10.6667V4.66667H11.3333V10.6667H9.33333Z"
      fill="black"
    />
  </svg>
);

export default SVG;
