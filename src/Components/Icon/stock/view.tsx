/* eslint-disable max-len */
import React from "react";
/**
 * Account Profile Icon
 * @param {Object} props Component props
 * @returns {React.Component} React component
 */
const SVG = (props: unknown): unknown => (
  <svg
    width="11"
    height="14"
    viewBox="0 0 11 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M0.680704 0.414862C0.894753 0.298001 1.15553 0.307342 1.36067 0.439217L10.694 6.43922C10.8848 6.56188 11.0002 6.77316 11.0002 7C11.0002 7.22685 10.8848 7.43812 10.694 7.56079L1.36067 13.5608C1.15553 13.6927 0.894753 13.702 0.680704 13.5851C0.466655 13.4683 0.333496 13.2439 0.333496 13V1C0.333496 0.756132 0.466655 0.531722 0.680704 0.414862ZM1.66683 2.22111V11.7789L9.10066 7L1.66683 2.22111Z"
      fill="white"
    />
  </svg>
);

export default SVG;
