/* eslint-disable max-len */
import React from "react";
/**
 * Account Profile Icon
 * @param {Object} props Component props
 * @returns {React.Component} React component
 */
const SVG = (props: unknown): unknown => (
  <svg
    width="16"
    height="16"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8.00016 2.66666C8.36835 2.66666 8.66683 2.96513 8.66683 3.33332V12.6667C8.66683 13.0348 8.36835 13.3333 8.00016 13.3333C7.63197 13.3333 7.3335 13.0348 7.3335 12.6667V3.33332C7.3335 2.96513 7.63197 2.66666 8.00016 2.66666Z"
      fill="white"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M2.6665 8.00001C2.6665 7.63182 2.96498 7.33334 3.33317 7.33334H12.6665C13.0347 7.33334 13.3332 7.63182 13.3332 8.00001C13.3332 8.3682 13.0347 8.66668 12.6665 8.66668H3.33317C2.96498 8.66668 2.6665 8.3682 2.6665 8.00001Z"
      fill="white"
    />
  </svg>
);

export default SVG;
