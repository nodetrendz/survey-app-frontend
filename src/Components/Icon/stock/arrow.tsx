/* eslint-disable max-len */
import React from "react";
/**
 * Account Profile Icon
 * @param {Object} props Component props
 * @returns {React.Component} React component
 */
const SVG = (props: unknown): unknown => (
  <svg
    width="16"
    height="16"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8.47124 2.86195C8.21089 2.6016 7.78878 2.6016 7.52843 2.86195C7.26808 3.1223 7.26808 3.54441 7.52843 3.80476L11.057 7.33335H3.33317C2.96498 7.33335 2.6665 7.63183 2.6665 8.00002C2.6665 8.36821 2.96498 8.66669 3.33317 8.66669H11.057L7.52843 12.1953C7.26808 12.4556 7.26808 12.8777 7.52843 13.1381C7.78878 13.3984 8.21089 13.3984 8.47124 13.1381L13.1379 8.47142C13.3983 8.21108 13.3983 7.78897 13.1379 7.52862L8.47124 2.86195Z"
      fill="#2E384D"
    />
  </svg>
);

export default SVG;
