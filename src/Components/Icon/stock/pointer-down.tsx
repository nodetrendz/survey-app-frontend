/* eslint-disable max-len */
import React from "react";
/**
 * Account Profile Icon
 * @param {Object} props Component props
 * @returns {React.Component} React component
 */
const SVG = (props: unknown): unknown => (
  <svg
    width="6"
    height="4"
    viewBox="0 0 6 4"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M0.703125 0.289078L3 2.58595L5.29688 0.289078L6 0.992203L3 3.9922L0 0.992203L0.703125 0.289078Z"
      fill="#8798AD"
    />
  </svg>
);

export default SVG;
