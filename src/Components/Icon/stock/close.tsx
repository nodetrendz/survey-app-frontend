/* eslint-disable max-len */
import React from "react";
/**
 * Account Profile Icon
 * @param {Object} props Component props
 * @returns {React.Component} React component
 */
const SVG = (props: unknown): unknown => (
  <svg
    width="16"
    height="16"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M0 2C0 0.895431 0.895431 0 2 0H14C15.1046 0 16 0.895431 16 2V14C16 15.1046 15.1046 16 14 16H2C0.895431 16 0 15.1046 0 14V2Z"
      fill="#8798AD"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M12.4716 3.52861C12.7319 3.78896 12.7319 4.21107 12.4716 4.47141L4.47157 12.4714C4.21122 12.7318 3.78911 12.7318 3.52876 12.4714C3.26841 12.2111 3.26841 11.789 3.52876 11.5286L11.5288 3.52861C11.7891 3.26826 12.2112 3.26826 12.4716 3.52861Z"
      fill="white"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M3.52876 3.52861C3.78911 3.26826 4.21122 3.26826 4.47157 3.52861L12.4716 11.5286C12.7319 11.789 12.7319 12.2111 12.4716 12.4714C12.2112 12.7318 11.7891 12.7318 11.5288 12.4714L3.52876 4.47141C3.26841 4.21107 3.26841 3.78896 3.52876 3.52861Z"
      fill="white"
    />
  </svg>
);

export default SVG;
