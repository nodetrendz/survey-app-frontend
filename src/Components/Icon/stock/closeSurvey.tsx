/* eslint-disable max-len */
import React from "react";
/**
 * Account Profile Icon
 * @param {Object} props Component props
 * @returns {React.Component} React component
 */
const SVG = (props: unknown): unknown => (
  <svg
    width="16"
    height="16"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M7.99984 2.00001C4.68613 2.00001 1.99984 4.6863 1.99984 8C1.99984 11.3137 4.68613 14 7.99984 14C11.3135 14 13.9998 11.3137 13.9998 8C13.9998 4.6863 11.3135 2.00001 7.99984 2.00001ZM0.666504 8C0.666504 3.94992 3.94975 0.666672 7.99984 0.666672C12.0499 0.666672 15.3332 3.94992 15.3332 8C15.3332 12.0501 12.0499 15.3333 7.99984 15.3333C3.94975 15.3333 0.666504 12.0501 0.666504 8Z"
      fill="#2E384D"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M5.3335 5.99999C5.3335 5.63181 5.63197 5.33333 6.00016 5.33333H10.0002C10.3684 5.33333 10.6668 5.63181 10.6668 5.99999V10C10.6668 10.3682 10.3684 10.6667 10.0002 10.6667H6.00016C5.63197 10.6667 5.3335 10.3682 5.3335 10V5.99999ZM6.66683 6.66666V9.33333H9.3335V6.66666H6.66683Z"
      fill="#2E384D"
    />
  </svg>
);

export default SVG;
