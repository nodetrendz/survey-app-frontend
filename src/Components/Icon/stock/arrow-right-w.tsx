/* eslint-disable max-len */
import React from "react";
/**
 * Account Profile Icon
 * @param {Object} props Component props
 * @returns {React.Component} React component
 */
const SVG = (props: unknown): unknown => (
  <svg
    width="16"
    height="16"
    viewBox="0 0 16 16"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M8.47124 2.86193C8.21089 2.60158 7.78878 2.60158 7.52843 2.86193C7.26808 3.12228 7.26808 3.54439 7.52843 3.80474L11.057 7.33334H3.33317C2.96498 7.33334 2.6665 7.63182 2.6665 8.00001C2.6665 8.3682 2.96498 8.66667 3.33317 8.66667H11.057L7.52843 12.1953C7.26808 12.4556 7.26808 12.8777 7.52843 13.1381C7.78878 13.3984 8.21089 13.3984 8.47124 13.1381L13.1379 8.47141C13.3983 8.21106 13.3983 7.78895 13.1379 7.5286L8.47124 2.86193Z"
      fill="white"
    />
  </svg>
);

export default SVG;
