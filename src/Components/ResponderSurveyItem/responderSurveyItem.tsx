import React, { useEffect, useState } from "react";
import Icon from "Components/Icon/icon";
import styles from "./responderSurveyItem.module.scss";
import ISurveyListItem from "Http/com/models/responses/coordinator/ISurveyListItem";
import { Link } from "react-router-dom";

const ResponderSurveyItem: React.FC<ISurveyListItem> = (
  props: ISurveyListItem
) => {
  const [route, setRoute] = useState("");

  useEffect(() => {
    let mounted = true;

    mounted &&
      props &&
      props.id &&
      setRoute(`/responder-surveyquestion/${props.id}`);

    return () => {
      mounted = false;
    };
  }, []);

  return (
    <div className={styles.responderSurveyItem}>
      <div className={styles.responderSurveyItem_text}>
        <h1> {props.title} ...</h1>
        <p>Date created: {props.created}</p>
      </div>
      <div className={styles.responderSurveyItem__status}>
        <p className={props.status === "open" ? styles.open : styles.close}>
          {props.status}
        </p>
      </div>
      <div className={styles.responderSurveyItem__more}>
        {props.status !== "open" ? (
          <Icon name="more" />
        ) : (
          <Link to={route}>
            <Icon name="more" />
          </Link>
        )}
      </div>
    </div>
  );
};

export default ResponderSurveyItem;
