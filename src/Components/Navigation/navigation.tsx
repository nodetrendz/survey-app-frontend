import Icon from "Components/Icon/icon";
import LogoutButton from "Components/LogoutButton/logoutButton";
import { composeClasses } from "Libs/Utils/utils";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import styles from "./navigation.module.scss";

export interface IProps {
  title: string;
  username: string;
  variantName: string;
}

const Navigation: React.FunctionComponent<IProps> = (props: IProps) => {
  const [showLogout, setShowLogout] = useState(false);

  return (
    <div className={styles.wrapper}>
      <div className={styles.navigation}>
        <div className={styles.navigation__title}>
          <Link to={props.variantName === "Respondent" ? "#" : "/"}>
            <Icon name="grid" />
          </Link>
          <h1>{props.title}</h1>
        </div>
        <div className={styles.navigation__userInfo}>
          <p>{`${props.variantName}, ${props.username}`}</p>
          <div
            className={styles.iconWrapper}
            onClick={() => setShowLogout(!showLogout)}
          >
            <Icon name="user" />
          </div>
          <div className={styles.navigation__userInfo_logoutButton}>
            <LogoutButton showLogout={showLogout} />
          </div>
        </div>
      </div>
    </div>
  );
};
export default Navigation;
