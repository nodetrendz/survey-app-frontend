import React, { useEffect } from "react";
import styles from "./responderOption.module.scss";

const ResponderOption: React.FC<any> = (props: any) => {

  return (
    <div className={styles.responderOption}>
      <label htmlFor="option">
        <input type="radio" value={props.option.id} name={props.name} className={styles.checkbox} onChange={(e: any) => props.onChange(e.target.value)} required />
        {props.option.value}
      </label>
    </div>
  );
};

export default ResponderOption;
