import ResponderSurveyItem from "Components/ResponderSurveyItem/responderSurveyItem";
import ISurveyListItem from "Http/com/models/responses/coordinator/ISurveyListItem";
import RespondentService from "Http/Service/RespondentService";
import React, { useEffect, useState } from "react";
import styles from "./surveyListResponder.module.scss";

const SurveyListResponder: React.FC = () => {

  const [surveys, setSurveys] = useState<Array<ISurveyListItem>>([]);

  const fetchSurveys = async () => {
    const data = await RespondentService.get_surveys();
    setSurveys(data);
  };

  useEffect(() => {
    let mounted = true;

    if (mounted) {
      fetchSurveys();
    }

    return () => {
      mounted = false;
    };

  }, []);

  return (
    <div className={styles.surveyListResponder}>
      <div className={styles.surveyListResponder__heading}>
        <h1>Survey List</h1>
        <div className={styles.surveyListResponder__select}>
          <select>
            <option>All</option>
          </select>
        </div>
      </div>
      <div className={styles.surveyListResponder__main}>
        <div className={styles.header}>
          <p className={styles.header__title}>Title</p>
          <p className={styles.header__status}>Status</p>
          <p className={styles.header__actions}>Actions</p>
        </div>
        <div className={styles.surveyListResponder__main_list}>
          {surveys.map((survey: ISurveyListItem, key: number) => <ResponderSurveyItem key={key} {...survey} />)}
        </div>
      </div>
    </div>
  );
};

export default SurveyListResponder;
