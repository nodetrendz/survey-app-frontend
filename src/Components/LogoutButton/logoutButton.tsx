import React from "react";
import Icon from "Components/Icon/icon";
import styles from "./logoutButton.module.scss";
import { composeClasses } from "Libs/Utils/utils";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";

export interface IProps {
  showLogout: boolean;
}
const LogoutButton: React.FunctionComponent<IProps> = ({ showLogout }) => {


  return (
    <Link
      to={'/logout'}
      className={composeClasses(
        styles.logoutButton,
        showLogout ? styles.showLogout : ""
      )}
    >
      <Icon name="log-out" />
      <p>Logout</p>
    </Link>
  );
};

export default LogoutButton;
