import React from "react";
import Loader from "react-loader-spinner";
import Icon from "Components/Icon/icon";
import styles from "./button.module.scss";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
interface IButtonType {
  className?: string;
  title?: string;
  isDisable?: boolean;
  isSubmitting?: boolean;
  handleClick?: any;
  value?: string;
  icon?: string;
  iconClass?: string;
  type?: any;
}
const defaultProps: IButtonType = {
  className: "",
  title: " ",
  isDisable: false,
  handleClick: () => {
    return null;
  },
  value: " ",
  isSubmitting: false,
  icon: "",
  iconClass: "",
};
const Button = ({
  className,
  title,
  icon,
  isSubmitting,
  isDisable,
  handleClick,
  value,
  type,
}: IButtonType): any => {
  return (
    <button
      className={styles.button}
      disabled={isDisable}
      onClick={handleClick}
      value={value}
      type={type ?? "spinner.tsx"}
    >
      {icon && (
        <div className={styles.icon}>
          <Icon name={`${icon}`} />
        </div>
      )}
      {isSubmitting ? <Loader type={"Circles"} color={"#FFFFFF"} height={30} width={30} />: title}
    </button>
  );
};

Button.defaultProps = defaultProps;
export default Button;
