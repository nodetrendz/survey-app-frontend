import Icon from "Components/Icon/icon";
import MoreActionsAdmin from "Components/MoreActionsAdmin/moreActionsAdmin";
import ISurvey from "Http/com/models/responses/coordinator/ISurvey";
import useClickOutSide from "CustomHooks/useClickOutside";
import { composeClasses } from "Libs/Utils/utils";
import React, { useEffect, useState } from "react";
import styles from "./surveyListAdminItem.module.scss";
import IMoreActionsData from "Models/IMoreActionsData";
import CoordinatorService from "Http/Service/CoordinatorService";

interface ISurveyListAdminItem {
  parameter?: ISurvey;
  id: any;
}

const SurveyListAdminItem: React.FC<ISurveyListAdminItem> = (
  props: ISurveyListAdminItem
) => {
  const [more, setMore] = useState<boolean>(false);
  const [title, setTitle] = useState<string>("");
  const [createdAt, setCreatedAt] = useState<string>("");
  const [modified, setModified] = useState<string>("");
  const [questions, setQuestions] = useState<number>(0);
  const [responses, setResponses] = useState<number>(0);
  const [status, setStatus] = useState<string>("");

  const handleCloseSurvey = async () => {
    const data = await CoordinatorService.close_survey(props.id);
    if (data === true) window.location.href = '/';
  };

  const composeMoreAction = (): Array<IMoreActionsData> => {
    return [
      {
        title: "Report",
        icon: "report",
        route: `/analytics/${props.id}`,
      },
        {
        title: "Close",
        icon: "closeSurvey",
        handleClick: handleCloseSurvey,
      },
    ];
  };

  useEffect(() => {
    let mounted = true;

    if (mounted && props && props.parameter) {
      props.parameter &&
        props.parameter.survey_name &&
        setTitle(props.parameter.survey_name);
      props.parameter &&
        props.parameter.date_created &&
        setCreatedAt(props.parameter.date_created);
      props.parameter &&
        props.parameter.date_modified &&
        setModified(props.parameter.date_modified);
      props.parameter &&
        props.parameter.questions &&
        setQuestions(props.parameter.questions);
      props.parameter &&
        props.parameter.responses &&
        setResponses(props.parameter.responses);
      props.parameter &&
        props.parameter.status &&
        setStatus(props.parameter.status);
    }

    return () => {
      mounted = false;
    };
  }, []);
  const moreActionsNode = useClickOutSide((value: any) => {
    setMore(value);
  }, props.id);

  return (
    <div className={styles.surveyListAdminItem}>
      <div className={styles.surveyListAdminItem__surveyTitle}>
        <p>{title}</p>
      </div>
      <div>
        <p>{createdAt}</p>
      </div>
      <div>
        <p>{modified}</p>
      </div>
      <div>
        <p>{questions}</p>
      </div>
      <div>
        <p>{responses}</p>
      </div>
      <div className={styles.surveyListAdminItem__status}>
        <div
          className={composeClasses(
            status === "open"
              ? styles.open
              : status === "closed"
              ? styles.closed
              : styles.draft
          )}
        ></div>
        <p>{status}</p>
      </div>
      <div ref={moreActionsNode} id={`${props.id}`} className={styles.moreIcon}>
        <Icon name="more" />
      </div>
      <div
        className={composeClasses(
          styles.surveyListAdminItem__moreActions,
          more ? styles.showMore : styles.hideMore
        )}
      >
        <MoreActionsAdmin moreActionsData={composeMoreAction()}/>
      </div>
    </div>
  );
};

export default SurveyListAdminItem;
