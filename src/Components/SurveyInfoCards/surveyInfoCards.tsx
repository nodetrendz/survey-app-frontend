import SurveyInfoCard from "Components/SurveryInfoCard/surveyInfoCard";
import CoordinatorService from "Http/Service/CoordinatorService";
import React, { Fragment, useEffect, useState } from "react";

const SurveyInfoCards: React.FC = () => {
  const [tickets, setTickets] = useState<any>(<Fragment />);

  const [ticketsCounter, setTicketsCounter] = useState<any>({
    open_surveys: {
      percentage: 0,
      score: 0,
    },
    closed_surveys: {
      percentage: 0,
      score: 0,
    },
  });

  const loadTicketsCounter = async () => {
    const counter: any = await CoordinatorService.fetch_my_surveys_counter();
    counter &&
      setTickets(
        <Fragment>
          <SurveyInfoCard
            title={"Open Surveys"}
            count={counter.open_surveys?.score || 0}
            percentage={counter.open_surveys?.percentage || 0}
            open={true}
          />
          <SurveyInfoCard
            title={"Closed Surveys"}
            count={counter.closed_surveys?.score || 0}
            percentage={counter.closed_surveys?.percentage || 0}
            open={false}
          />
        </Fragment>
      );
  };

  useEffect(() => {
    let mounted = true;

    if (mounted) {
      loadTicketsCounter();
    }

    return () => {
      mounted = false;
    };
  }, []);

  return <Fragment>{tickets}</Fragment>;
};

export default SurveyInfoCards;
