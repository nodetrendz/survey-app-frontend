import Icon from "Components/Icon/icon";
import ISurveyContext from "Http/com/context/ISurveyContext";
import { NotificationContext } from "Http/com/context/NotificationContext";
import SurveyContext from "Http/com/context/SurveyContext";
import { composeClasses } from "Libs/Utils/utils";

import React, { Fragment, useContext, useEffect, useState } from "react";
import styles from "./notification.module.scss";

const Notification: React.FC = () => {
  const context: ISurveyContext = useContext(SurveyContext);
  const [text, setText] = useState<string>("");
  const [status, setStatus] = useState<boolean | undefined>(undefined);
  const { state, dispatch } = useContext(NotificationContext);

  useEffect(() => {
    let mounted = true;

    if (mounted && context && context.notification) {
      context.notification.text && setText(context.notification.text);
      context.notification.status && setStatus(context.notification.status);
      dispatch({ type: "SET_SUCCESS", payload: status });
      dispatch({ type: "SET_ERROR", payload: !status });
    }

    setTimeout(() => {
      dispatch({ type: "SET_SUCCESS", payload: false });
      dispatch({ type: "SET_ERROR", payload: false });
    }, 3000);
    return () => {
      mounted = false;
    };
  }, [context.notification]);

  console.log(state, "status");
  return status !== undefined ? (
    <div
      className={composeClasses(
        styles.notification,
        status ? styles.notification__success : styles.notification__error
      )}
    >
      {status && (
        <>
          <Icon name="notification-success" />
          <div className={styles.notification__text}>
            <h1>Successful</h1>
            <p>{text}</p>
          </div>
          <div className={styles.notification__close}>
            <Icon name="notification-close" />
          </div>
        </>
      )}
      {!status && (
        <>
          <Icon name="notificationError" />
          <div className={styles.notification__text}>
            <h1>Error</h1>
            <p>{text}</p>
          </div>
          <div className={styles.notification__close}>
            <Icon name="notification-close" />
          </div>
        </>
      )}
    </div>
  ) : (
    <Fragment />
  );
};

export default Notification;
