import React, { useEffect, useState } from "react";
import Icon from "Components/Icon/icon";
import styles from "./responderQuestion.module.scss";
import ResponderOption from "Components/ResponderOption/responderOption";


const ResponderQuestion: React.FC<any> = (props: any) => {

  const [question, setQuestion] = useState<string>('');
  const [options, setOptions] = useState<Array<any>>([]);
  const [id, setId] = useState<Array<any>>([]);
  const [selected, setSelected] = useState<any>('');

  useEffect(() => {

    let mounted = true;

    if (mounted && props) {
      setId(props.index || '');
      setQuestion(props.question.text || '');
      setOptions(props.question.options || []);
    }

    return () => {
      mounted = false;
    };

  }, [props]);


  useEffect(() => {

    let mounted = true;

    if (mounted && props && selected) {
      props.onChange(props.question.id, selected);
    }

    return () => {
      mounted = false;
    };

  }, [selected]);

  return (
    <div className={styles.responderQuestion}>
      <div className={styles.responderQuestion__heading}>
        <p>{id}</p>
        <Icon name="arrow" />
        <p>{question}</p>
      </div>
      <div className={styles.responderQuestion__options}>
        {options && options.map((option: any, key: number) => <ResponderOption key={key} option={option} name={`option${props.index}`} onChange={setSelected} />)}
      </div>
    </div>
  );
};

export default ResponderQuestion;
