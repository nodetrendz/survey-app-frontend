import Button from "Components/Button/button";
import Option from "Components/Option/option";
import ISurveyContext from "Http/com/context/ISurveyContext";
import SurveyContext from "Http/com/context/SurveyContext";
import OptionModel from "Http/com/models/requests/coordinator/formModels/OptionModel";
import React, { useContext, useEffect, useState } from "react";
import styles from "./question.module.scss";

const OPTIONS_LIMIT = 5;

interface IQuestion {
  index: number;
  onChange: Function;
}

const Question: React.FC<IQuestion> = (props: IQuestion) => {

  const context: ISurveyContext = useContext(SurveyContext);

  const [text, setText] = useState<string>('');
  const [options, setOptions] = useState<Array<any>>([]);
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);


  /**
   * Method to handle the change event fired from options
   */
  const handleOptionChangeEvent = (changeEvent: any, changeIndex: number) => {
    changeIndex -= 1; // because we are access the index of an array
  };

  const addOption = () => {
    const tempOptions = [...options];

    if (currentIndex >= OPTIONS_LIMIT) return;

    context.questionModels[props.index - 1].options[currentIndex] = new OptionModel();;
  
    tempOptions.push(<Option key={currentIndex + 1} index={currentIndex + 1} parentIndex={props.index} handleChange={handleOptionChangeEvent} />);
    setOptions([...tempOptions]);
    setCurrentIndex(currentIndex + 1);
  };

  const handleAddOptionClickEvent = (clickEvent: any) => {
    clickEvent.preventDefault();
    addOption();
  }; 

  useEffect(() => {
    let mounted = true;

    if (mounted && context) {
      addOption();
    }

    return () => {
      mounted = false;
    };
  }, []);

  useEffect(() => {
    let mounted = true;

    if (mounted && props && props.index) {
      context.questionModels[props.index - 1].text = text;
    }

    return () => {
      mounted = false;
    };
  }, [text]);


  return (
    <div className={styles.question}>
      <div className={styles.question__input}>
        <label htmlFor="question">Question {props.index}</label>
        <input
          type="text"
          name="question"
          onChange={(e: any) => setText(e.target.value)}
          placeholder="Enter your question here"
          required={true}
        />
      </div>

      <div className={styles.question__options}>
        <div className={styles.question__options_addOptionsBtn}>
          <Button title="Add options" icon="plus" handleClick={handleAddOptionClickEvent} />
        </div>
        {options}
      </div>
    </div>
  );
};

export default Question;
