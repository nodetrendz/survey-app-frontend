import React from "react";
import SpinnerGif from "Assets/images/spinner.gif";
import styles from "./spinner.module.scss";

const Spinner: React.FC = () =>
    <div className={styles.main}>
      <img src={SpinnerGif} alt="spinner image" />;
    </div>;

export default Spinner;
