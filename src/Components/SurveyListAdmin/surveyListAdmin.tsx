import Icon from "Components/Icon/icon";
import SurveyListAdminItem from "Components/SurveyListAdminItem/surveyListAdminItem";
import React, { useEffect, useState } from "react";
import styles from "./surveyListAdmin.module.scss";
interface IProps {
  setSurveys: any;
  surveys: any;
}

const SurveyListAdmin: React.FC<IProps> = (props: IProps) => {
  return (
    <div className={styles.surveyListAdmin}>
      <div className={styles.surveyListAdmin__header}>
        <h1>Survey List</h1>
        <div className={styles.surveyListAdmin__header_select}>
          <select>
            <option>All</option>
          </select>
          {<Icon name="pointer-down" className={styles.pointer_down} />}
        </div>
      </div>
      <div className={styles.surveyListAdmin__mainContent}>
        <div className={styles.surveyListAdmin__mainContent__header}>
          <p>Survey Name</p>
          <p>Date Created</p>
          <p>Date Modified</p>
          <p>Questions</p>
          <p>Responses</p>
          <p>Status</p>
          <p>Actions</p>
        </div>
        <div>
          {props.surveys.map((survey: any, index: number) => (
            <SurveyListAdminItem key={index} parameter={survey} id={survey.id} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default SurveyListAdmin;
