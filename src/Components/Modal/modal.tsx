/* eslint-disable @typescript-eslint/ban-types */
import React, { Fragment, useContext, useState } from "react";
import Icon from "Components/Icon/icon";
import styles from "./modal.module.scss";
import { useHistory } from "react-router";
import { composeClasses } from "Libs/Utils/utils";
import Button from "Components/Button/button";
import ISurveyContext from "Http/com/context/ISurveyContext";
import SurveyContext from "Http/com/context/SurveyContext";

export interface IModal {
  close: boolean;
  setClose: any;
}
const Modal: React.FunctionComponent<IModal> = ({ close, setClose }) => {
  const context: ISurveyContext = useContext(SurveyContext);

  const history = useHistory();

  const [name, setName] = useState<string>('');

  const linktoCreateSurvey = (e: any) => {
    context.title = name;
    history.push("/create-survey");
  }

  const handleBackdropClick = (event: any, backdropRef: any) => {
    if (event.target && event.target === backdropRef) {
      setClose(true);
    }
  };

  let backdropRef: HTMLDivElement | null;
  const className = composeClasses(
    styles.modal,
    close ? styles.modalHidden : " "
  );
  return (
    <Fragment>
      <div
        className={!close ? styles.overlay : undefined}
        onClick={(event) => handleBackdropClick(event, backdropRef)}
        ref={(node) => (backdropRef = node)}
      />
      {!close && (
        <section className={styles.modal}>
          <div className={styles.modal__header}>
            <h1>Survey Title</h1>
            <div className={styles.closeIcon} onClick={() => setClose(true)}>
              <Icon name="close" />
            </div>
          </div>
          <div className={styles.modal__mainContent}>
            <div className={styles.modal__mainContent__input}>
              <label htmlFor="surveytitle">Survey Title</label>
              <input className={styles.modal__mainContent__input__entry} type="text" name="surveytitle" onChange={(e:any) => setName(e.target.value)} />
            </div>
            {/* <div className={styles.modal__mainContent__select}>
              <label htmlFor="surveytitle">Type of survey</label>
              <select name="surveytitle">
                <option>Select Survey Category</option>
              </select>
            </div> */}
            <div className={styles.modal__mainContent__button_continue}>
              <Button
                title="continue"
                icon="arrow-right-w"
                handleClick={linktoCreateSurvey}
              />
            </div>
          </div>
        </section>
      )}
    </Fragment>
  );
};

export default Modal;
